import { HttpModule, HttpService } from '@nestjs/axios';
import { ConfigService } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { AppConfigModule } from '../config/config.module';
import { CurrencyCallsService } from '../domain/currency-calls/currency-calls.service';
import { ExchangeService } from '../domain/exchange/exchange.service';
import { CurrencyService } from './currency.service';

describe('Currency Service', () => {
    let app: TestingModule;
    let http: HttpService;
    let service: CurrencyService;

    beforeAll(async () => {
        app = await Test.createTestingModule({
            imports: [
                AppConfigModule,
                HttpModule.registerAsync({
                    inject: [ConfigService],
                    useFactory: (config: ConfigService) => {
                        return {
                            baseURL: 'https://api.currencyapi.com/v3',
                            timeout: config.get<number>('TIME_OUT') * 1000,
                            headers: {
                                apikey: config.get<string>('API_KEY'),
                            },
                        };
                    },
                }),
            ],
            providers: [
                CurrencyService,
                {
                    provide: ExchangeService,
                    useValue: { bulkInsert: jest.fn() },
                },
                {
                    provide: CurrencyCallsService,
                    useValue: { insert: jest.fn() },
                },
            ],
        }).compile();

        service = app.get<CurrencyService>(CurrencyService);
        http = app.get<HttpService>(HttpService);
    });

    afterAll((done) => {
        app.close().then(() => {
            done();
        });
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });

    it('should be an instance of CurrencyService', () => {
        expect(service).toBeInstanceOf(CurrencyService);
    });
});
