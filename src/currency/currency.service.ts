import { HttpService } from '@nestjs/axios';
import { Injectable, Logger } from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { catchError, map, switchMap } from 'rxjs';
import { CurrencyCallsService } from '../domain/currency-calls/currency-calls.service';
import { CurrencyCallStatus } from '../domain/currency-calls/interfaces/currency-call.interface';
import { ExchangeService } from '../domain/exchange/exchange.service';
import { Exchange } from '../domain/exchange/schemas/exchange.schema';
import { CurrencyServiceApi } from './interfaces/currency.api';
import { LatestResponse } from './interfaces/latest.response';

@Injectable()
export class CurrencyService implements CurrencyServiceApi {
    private readonly logger = new Logger(CurrencyService.name);

    constructor(
        private readonly http: HttpService,
        private readonly exchangeService: ExchangeService,
        private readonly currencyCallService: CurrencyCallsService,
    ) {}

    latest() {
        let time = performance.now();

        return this.http
            .get<LatestResponse>('/latest')
            .pipe(
                map((res) => {
                    return Object.values(res.data.data).map(({ code, value }) =>
                        plainToClass(Exchange, {
                            code,
                            value,
                            last_updated_at: new Date(),
                        }),
                    );
                }),
                switchMap((dtoList) => {
                    return this.exchangeService.bulkInsert(dtoList);
                }),
                switchMap(() => {
                    time = (performance.now() - time) / 1000;
                    this.logger.debug(`TimeLapse ${time} seconds`);
                    return this.currencyCallService.insert(
                        time,
                        CurrencyCallStatus.COMPLETED,
                    );
                }),
                catchError(() => {
                    time = (performance.now() - time) / 1000;
                    this.logger.debug(`TimeLapse ${time} seconds`);
                    return this.currencyCallService.insert(
                        time,
                        CurrencyCallStatus.TIMEOUT,
                    );
                }),
            )
            .subscribe();
    }
}
