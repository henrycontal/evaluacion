export interface LatestResponse {
    meta: Meta;
    data: { [key: string]: Datum };
}

export interface Datum {
    code: string;
    value: number;
}

export interface Meta {
    last_updated_at: Date;
}
