import { Controller, Get, Logger, Param } from '@nestjs/common';
import { AppService } from './app.service';
import { Range, RangeType } from './decorators/range.decorator';
import { CurrencyPipe } from './pipes/currency.pipe';

@Controller('currencies')
export class AppController {
    private readonly logger: Logger = new Logger();

    constructor(private readonly appService: AppService) {}

    @Get(':currency')
    public getCurrency(
        @Param('currency', new CurrencyPipe()) currency: string,
        @Range() ranges: RangeType,
    ) {
        this.logger.log(`CURRENCY - ${currency}`);

        if (currency === 'ALL') {
            return this.appService.getCurrencies(ranges);
        }

        return this.appService.getCurrency(currency, ranges);
    }
}
