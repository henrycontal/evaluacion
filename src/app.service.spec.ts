import { Test, TestingModule } from '@nestjs/testing';
import { AppService } from './app.service';
import { ExchangeService } from './domain/exchange/exchange.service';

describe('AppService', () => {
    let app: TestingModule;
    let service: AppService;
    let exchangeService: ExchangeService;

    beforeAll(async () => {
        app = await Test.createTestingModule({
            providers: [
                AppService,
                {
                    provide: ExchangeService,
                    useValue: {
                        find: jest.fn(),
                        findOneByCode: jest.fn(),
                    },
                },
            ],
        }).compile();

        service = app.get<AppService>(AppService);
        exchangeService = app.get<ExchangeService>(ExchangeService);
    });

    afterAll((done) => {
        app.close().then(() => {
            done();
        });
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });

    it('should be an instance of AppService', () => {
        expect(service).toBeInstanceOf(AppService);
    });

    it('should call find from exchangeService', async () => {
        await service.getCurrencies({});
        expect(exchangeService.find).toBeCalled();
    });

    it('should call findOneByCode from exchangeService', async () => {
        await service.getCurrency('MXN', {});
        expect(exchangeService.findOneByCode).toBeCalled();
    });
});
