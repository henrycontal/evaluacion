import {
    ArgumentMetadata,
    BadRequestException,
    Injectable,
    PipeTransform,
} from '@nestjs/common';

@Injectable()
export class CurrencyPipe implements PipeTransform {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    transform(value: string, _: ArgumentMetadata) {
        const lowerCaseValue = value.toLocaleUpperCase();

        const threeLettersRegExp = new RegExp(/^[A-Z]{3}$/g);

        if (!threeLettersRegExp.exec(lowerCaseValue)) {
            throw new BadRequestException();
        }

        return lowerCaseValue;
    }
}
