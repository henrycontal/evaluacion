import { ArgumentMetadata } from '@nestjs/common';
import { CurrencyPipe } from './currency.pipe';

describe('Currency Pipe', () => {
    let target: CurrencyPipe;
    const metadata: ArgumentMetadata = {
        type: 'body',
        data: '',
    };

    beforeAll(() => {
        target = new CurrencyPipe();
    });

    it('should return the currency value into uppercase', () => {
        expect(target.transform('mxn', metadata)).toBe('MXN');
    });

    it('should return an exception with value 123', () => {
        expect(() => target.transform('123', metadata)).toThrow();
    });
});
