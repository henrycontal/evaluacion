import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from './app.module';
import { CronService } from './cron/cron.service';

describe('App Module', () => {
    let app: TestingModule;
    let module: AppModule;
    let service: CronService;

    beforeAll(async () => {
        app = await Test.createTestingModule({
            imports: [AppModule],
        })
            .overrideProvider(CronService)
            .useValue({
                setTimeOut: jest.fn(),
                setInterval: jest.fn(),
                clearJobs: jest.fn(),
            })
            .compile();

        module = app.get<AppModule>(AppModule);
        service = app.get<CronService>(CronService);
    });

    afterAll((done) => {
        app.close().then(() => {
            done();
        });
    });

    it('should be defined', () => {
        expect(module).toBeDefined();
    });

    it('should execute setTimeOut and setInterval when onApplicationBootstrap', () => {
        module.onApplicationBootstrap();
        expect(service.setTimeOut).toBeCalled();
        expect(service.setInterval).toBeCalled();
    });

    it('should execute clearJobs when BeforeApplicationShutdown', () => {
        module.beforeApplicationShutdown();
        expect(service.clearJobs).toBeCalled();
    });
});
