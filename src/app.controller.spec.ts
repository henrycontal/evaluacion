import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { AppService } from './app.service';

describe('AppController', () => {
    let app: TestingModule;
    let controller: AppController;
    let service: AppService;

    beforeAll(async () => {
        app = await Test.createTestingModule({
            controllers: [AppController],
            providers: [
                {
                    provide: AppService,
                    useValue: {
                        getCurrency: jest.fn(),
                        getCurrencies: jest.fn(),
                    },
                },
            ],
        }).compile();

        service = app.get<AppService>(AppService);
        controller = app.get<AppController>(AppController);
    });

    afterAll((done) => {
        app.close().then(() => {
            done();
        });
    });

    describe('root', () => {
        it('should be defined', () => {
            expect(controller).toBeDefined();
        });

        it('should be an instance of AppController', () => {
            expect(controller).toBeInstanceOf(AppController);
        });

        it('should call getCurrencies from appService', async () => {
            await controller.getCurrency('ALL', {});
            expect(service.getCurrencies).toBeCalled();
        });

        it('should call getCurrency from appService', async () => {
            await controller.getCurrency('MXN', {});
            expect(service.getCurrency).toBeCalled();
        });
    });
});
