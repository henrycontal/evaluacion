import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { plainToInstance } from 'class-transformer';
import { Repository, SelectQueryBuilder } from 'typeorm';
import { RangeType } from '../../decorators/range.decorator';
import { ExchangeReadDTO } from './dtos/exchange-read.dto';
import { IExchange } from './interfaces/exchange.interface';
import { Exchange } from './schemas/exchange.schema';

@Injectable()
export class ExchangeService {
    constructor(
        @InjectRepository(Exchange)
        private readonly repository: Repository<Exchange>,
    ) {}

    public async bulkInsert(dtoList: IExchange[]) {
        await this.repository.manager.transaction(async (manager) => {
            for (const dto of dtoList) {
                await manager.save(dto);
            }
        });
    }

    public async find(range: RangeType) {
        let qs = this.repository.createQueryBuilder('ex');

        qs = this.setRangesToQuery(qs, range);

        const res = await qs.orderBy('ex.code', 'ASC').getMany();

        const dtos = plainToInstance(ExchangeReadDTO, res, {
            excludeExtraneousValues: true,
        });

        return this.groupByCode(dtos);
    }

    public async findOneByCode(code: string, range: RangeType) {
        let qs = this.repository
            .createQueryBuilder('ex')
            .where('ex.code = :code', { code });

        qs = this.setRangesToQuery(qs, range);

        const res = await qs.getMany();

        if (res.length === 0) {
            throw new NotFoundException();
        }

        const dtos = plainToInstance(ExchangeReadDTO, res, {
            excludeExtraneousValues: true,
        });

        return this.groupByCode(dtos);
    }

    private setRangesToQuery(
        query: SelectQueryBuilder<Exchange>,
        range: RangeType,
    ) {
        if (range?.finit) {
            const finit = new Date(range.finit);
            query = query.andWhere(`ex.last_updated_at >= :finit`, { finit });
        }

        if (range?.fend) {
            const fend = new Date(range.fend);
            query = query.andWhere(`ex.last_updated_at <= :fend`, { fend });
        }

        return query;
    }

    private groupByCode(dtos: ExchangeReadDTO[]) {
        return dtos.reduce((group, dto) => {
            const { code } = dto;

            if (group[code]) {
                group[code].push(dto);
            } else {
                group[code] = [dto];
            }

            return group;
        }, {});
    }
}
