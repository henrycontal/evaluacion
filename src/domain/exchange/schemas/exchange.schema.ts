import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { IExchange } from '../interfaces/exchange.interface';

@Entity({ name: 'exchanges' })
export class Exchange implements IExchange {
    @PrimaryGeneratedColumn({ type: 'bigint' })
    id: number;

    @Column()
    code: string;

    @Column({ type: 'float' })
    value: number;

    @Column({ type: 'timestamp' })
    last_updated_at: Date;
}
