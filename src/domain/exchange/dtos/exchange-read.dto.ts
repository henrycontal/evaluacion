import { Exclude, Expose, Transform } from 'class-transformer';
import { DateTime } from 'luxon';
import { IExchange } from '../interfaces/exchange.interface';

export class ExchangeReadDTO implements IExchange {
    @Expose()
    code: string;

    @Expose()
    value: number;

    @Exclude()
    last_updated_at: Date;

    @Expose()
    @Transform(({ obj }) => DateTime.fromJSDate(obj.last_updated_at))
    lastUpdatedAt: DateTime;
}
