import { NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { plainToInstance } from 'class-transformer';
import { Repository } from 'typeorm';
import { ExchangeService } from './exchange.service';
import { IExchange } from './interfaces/exchange.interface';
import { Exchange } from './schemas/exchange.schema';

describe('ExchangeService', () => {
    let app: TestingModule;
    let service: ExchangeService;
    let repository: Repository<Exchange>;

    beforeAll(async () => {
        app = await Test.createTestingModule({
            providers: [
                ExchangeService,
                {
                    provide: getRepositoryToken(Exchange),
                    useValue: { manager: jest.fn() },
                },
            ],
        }).compile();

        service = app.get<ExchangeService>(ExchangeService);
        repository = app.get<Repository<Exchange>>(
            getRepositoryToken(Exchange),
        );
    });

    afterAll((done) => {
        app.close().then(() => {
            done();
        });
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });

    it('should call save once', async () => {
        const dtoList: IExchange[] = [
            {
                code: 'MXN',
                value: 123,
                last_updated_at: new Date('2023-10-12T13:35:45'),
            },
        ];

        const mockTransactionManager = { save: jest.fn() };

        repository.manager.transaction = jest.fn().mockImplementation((cb) => {
            return cb(mockTransactionManager);
        });

        await service.bulkInsert(dtoList);

        expect(mockTransactionManager.save).toHaveBeenCalledTimes(
            dtoList.length,
        );
    });

    it('should find and group exchanges', async () => {
        const range = {
            finit: '2023-01-01T12:10:09',
            fend: '2023-12-31T12:10:09',
        };

        const queryBuilder = {
            orderBy: jest.fn().mockReturnThis(),
            andWhere: jest.fn().mockReturnThis(),
            getMany: jest.fn().mockResolvedValue(
                plainToInstance(Exchange, [
                    {
                        code: 'USD',
                        value: 123,
                        lastUpdatedAt: new Date('2023-10-12T12:20:10'),
                    },
                    {
                        code: 'EUR',
                        value: 456,
                        lastUpdatedAt: new Date('2023-10-12T12:20:10'),
                    },
                    {
                        code: 'EUR',
                        value: 789,
                        lastUpdatedAt: new Date('2023-10-12T12:20:10'),
                    },
                ]),
            ),
        };

        repository.createQueryBuilder = jest.fn().mockReturnValue(queryBuilder);

        const result = await service.find(range);

        expect(result).toHaveProperty('USD');
        expect(result['USD'].length).toBe(1);
        expect(result).toHaveProperty('EUR');
        expect(result['EUR'].length).toBe(2);
    });

    it('should find and group a single exchange', async () => {
        const code = 'USD';
        const range = {
            finit: '2023-01-01T00:00:00',
            fend: '2023-12-31T59:59:59',
        };

        const queryBuilder = {
            where: jest.fn().mockReturnThis(),
            andWhere: jest.fn().mockReturnThis(),
            getMany: jest.fn().mockResolvedValue([
                {
                    code: 'USD',
                    value: 100,
                    lastUpdatedAt: new Date('2023-10-12T12:20:10'),
                },
            ]),
        };

        repository.createQueryBuilder = jest.fn().mockReturnValue(queryBuilder);

        const result = await service.findOneByCode(code, range);

        expect(result).toHaveProperty('USD');
        expect(result['USD'].length).toBe(1);
    });

    it('should throw a NotFoundException', async () => {
        const code = 'AAA';
        const range = {
            finit: '2023-01-01T00:00:00',
            fend: '2023-12-31T59:59:59',
        };

        const queryBuilder = {
            where: jest.fn().mockReturnThis(),
            andWhere: jest.fn().mockReturnThis(),
            getMany: jest.fn().mockResolvedValue([]),
        };

        repository.createQueryBuilder = jest.fn().mockReturnValue(queryBuilder);

        try {
            await service.findOneByCode(code, range);
        } catch (error) {
            expect(error).toBeInstanceOf(NotFoundException);
        }
    });
});
