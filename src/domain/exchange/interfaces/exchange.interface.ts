export interface IExchange {
    code: string;
    value: number;
    last_updated_at: Date;
}
