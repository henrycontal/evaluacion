import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ExchangeService } from './exchange.service';
import { Exchange } from './schemas/exchange.schema';

@Module({
    imports: [TypeOrmModule.forFeature([Exchange])],
    providers: [ExchangeService],
    exports: [TypeOrmModule, ExchangeService],
})
export class ExchangeModule {}
