import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CurrencyCallsService } from './currency-calls.service';
import { CurrencyCall } from './schemas/currency-call.schema';

@Module({
    imports: [TypeOrmModule.forFeature([CurrencyCall])],
    providers: [CurrencyCallsService],
    exports: [TypeOrmModule, CurrencyCallsService],
})
export class CurrencyCallsModule {}
