import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Subscription, from } from 'rxjs';
import { InsertResult, Repository } from 'typeorm';

import { CurrencyCallsService } from './currency-calls.service';
import { CurrencyCallStatus } from './interfaces/currency-call.interface';
import { CurrencyCall } from './schemas/currency-call.schema';

describe('Currency Calls Service', () => {
    let app: TestingModule;
    let service: CurrencyCallsService;
    let repository: Repository<CurrencyCall>;
    let $subscription: Subscription = null;

    const result: InsertResult = {
        generatedMaps: [],
        identifiers: [],
        raw: {},
    };

    beforeAll(async () => {
        app = await Test.createTestingModule({
            providers: [
                CurrencyCallsService,
                {
                    provide: getRepositoryToken(CurrencyCall),
                    useValue: {
                        insert: jest.fn(),
                    },
                },
            ],
        }).compile();

        service = app.get<CurrencyCallsService>(CurrencyCallsService);
        repository = app.get<Repository<CurrencyCall>>(
            getRepositoryToken(CurrencyCall),
        );
    });

    afterAll((done) => {
        $subscription?.unsubscribe();
        app.close().then(() => {
            done();
        });
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });

    it('should insert a test currency call', () => {
        (repository.insert as jest.Mock).mockResolvedValue(result);

        $subscription = from(
            service.insert(123, CurrencyCallStatus.TEST),
        ).subscribe((res) => {
            expect(res).toEqual(result);
        });
    });

    it('should not insert a test currency call', () => {
        (repository.insert as jest.Mock).mockRejectedValue({});

        $subscription = from(
            service.insert(123, CurrencyCallStatus.TEST),
        ).subscribe((res) => {
            expect(res).toBeNull();
        });
    });
});
