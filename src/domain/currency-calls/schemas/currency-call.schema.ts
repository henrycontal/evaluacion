import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import {
    CurrencyCallStatus,
    ICurrencyCall,
} from '../interfaces/currency-call.interface';

@Entity({ name: 'currency_calls' })
export class CurrencyCall implements ICurrencyCall {
    @PrimaryGeneratedColumn({ type: 'int' })
    id: number;

    @Column({ type: 'timestamp' })
    call_date: Date;

    @Column({ type: 'float' })
    response_time: number;

    @Column({ type: 'varchar' })
    status: CurrencyCallStatus;
}
