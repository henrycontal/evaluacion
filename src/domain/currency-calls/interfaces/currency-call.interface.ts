export enum CurrencyCallStatus {
    COMPLETED = 'completed',
    TIMEOUT = 'timeout',
    TEST = 'test',
}

export interface ICurrencyCall {
    call_date: Date;
    response_time: number;
    status: CurrencyCallStatus;
}
