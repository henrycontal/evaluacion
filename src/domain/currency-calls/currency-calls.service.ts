import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import {
    CurrencyCallStatus,
    ICurrencyCall,
} from './interfaces/currency-call.interface';
import { CurrencyCall } from './schemas/currency-call.schema';

@Injectable()
export class CurrencyCallsService {
    private readonly logger = new Logger();

    constructor(
        @InjectRepository(CurrencyCall)
        private readonly repository: Repository<CurrencyCall>,
    ) {}

    public async insert(responseTime: number, status: CurrencyCallStatus) {
        const dto: ICurrencyCall = {
            call_date: new Date(),
            response_time: responseTime,
            status,
        };

        try {
            const $ = await this.repository.insert(dto);
            this.logger.debug(`Execution inserted into database`);
            return $;
        } catch (error) {
            this.logger.error(
                `Something went wrong while inserting into databse`,
            );
            return null;
        }
    }
}
