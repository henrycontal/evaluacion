import { Module } from '@nestjs/common';
import { CurrencyCallsModule } from './currency-calls/currency-calls.module';
import { ExchangeModule } from './exchange/exchange.module';

const modules = [CurrencyCallsModule, ExchangeModule];

@Module({
    imports: modules,
    exports: modules,
})
export class DomainModule {}
