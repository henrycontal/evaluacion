import { ROUTE_ARGS_METADATA } from '@nestjs/common/constants';
import { ExecutionContextHost } from '@nestjs/core/helpers/execution-context-host';
import * as httpMock from 'node-mocks-http';
import { Range, RangeType } from './range.decorator';

// eslint-disable-next-line @typescript-eslint/no-unused-vars, @typescript-eslint/ban-types
function getParamDecoratorFactory(decorator: Function) {
    class TestDecorator {
        public test(@Range() value: RangeType) {
            return value;
        }
    }
    const args = Reflect.getMetadata(
        ROUTE_ARGS_METADATA,
        TestDecorator,
        'test',
    );
    return args[Object.keys(args)[0]].factory;
}

describe('Range Decorator', () => {
    it('should throw an exception when the finit is invalid', () => {
        const req = httpMock.createRequest({
            query: {
                finit: 'asdas',
            },
        });
        const res = httpMock.createResponse();
        const ctx = new ExecutionContextHost([req, res]);
        const factory = getParamDecoratorFactory(Range);

        expect(() => factory(null, ctx)).toThrow();
    });

    it('should throw an exception when the fend is invalid', () => {
        const req = httpMock.createRequest({
            query: {
                fend: 'asdas',
            },
        });
        const res = httpMock.createResponse();
        const ctx = new ExecutionContextHost([req, res]);
        const factory = getParamDecoratorFactory(Range);

        expect(() => factory(null, ctx)).toThrow();
    });

    it('should throw an exception because finit > fend', () => {
        const req = httpMock.createRequest({
            query: {
                finit: '2023-01-02T12:00:00',
                fend: '2023-01-01T12:00:00',
            },
        });
        const res = httpMock.createResponse();
        const ctx = new ExecutionContextHost([req, res]);
        const factory = getParamDecoratorFactory(Range);

        expect(() => factory(null, ctx)).toThrow();
    });

    it('should get the ranges', () => {
        const params = {
            finit: '2023-01-01T12:00:00',
            fend: '2023-01-02T12:00:00',
        };
        const req = httpMock.createRequest({
            query: params,
        });
        const res = httpMock.createResponse();
        const ctx = new ExecutionContextHost([req, res]);
        const factory = getParamDecoratorFactory(Range);

        expect(factory(null, ctx)).toStrictEqual(params);
    });
});
