import {
    BadRequestException,
    ExecutionContext,
    createParamDecorator,
} from '@nestjs/common';
import { Request } from 'express';
import { DateTime } from 'luxon';

export interface RangeType {
    finit?: string;
    fend?: string;
}

export const Range = createParamDecorator(
    (_: unknown, ctx: ExecutionContext): RangeType => {
        const request = ctx
            .switchToHttp()
            .getRequest<
                Request<
                    unknown,
                    unknown,
                    unknown,
                    { finit: string; fend: string }
                >
            >();

        const finit = request?.query?.finit;
        const fend = request?.query?.fend;

        const dtInitDate = DateTime.fromISO(finit);
        const dtEndDate = DateTime.fromISO(fend);

        if ((finit && !dtInitDate.isValid) || (fend && !dtEndDate.isValid)) {
            throw new BadRequestException();
        }

        if (finit && fend) {
            const DIFF_STAMP = dtEndDate.diff(dtInitDate, 'milliseconds');

            if (DIFF_STAMP.milliseconds < 0) {
                throw new BadRequestException();
            }
        }

        return { finit, fend };
    },
);
