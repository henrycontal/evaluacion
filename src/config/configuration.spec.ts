import configuration from './configuration';

describe('Configuration', () => {
    it('should be a function', () => {
        expect(typeof configuration).toBe('function');
    });
});
