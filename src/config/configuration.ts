export default () => ({
    API_KEY: process.env.CURRENCY_API_KEY,
    TIME_INTERVAL: process.env.TIME_INTERVAL_MIN,
    TIME_OUT: process.env.TIME_OUT_SEG,

    // DATABASE
    DB_HOST: process.env.DB_HOST,
    DB_PORT: process.env.DB_PORT,
    DB_USER: process.env.DB_USER,
    DB_PASS: process.env.DB_PASS,
    DB_NAME: process.env.DB_NAME,
});
