import { Injectable } from '@nestjs/common';
import { RangeType } from './decorators/range.decorator';
import { ExchangeService } from './domain/exchange/exchange.service';

@Injectable()
export class AppService {
    constructor(private readonly exchangeService: ExchangeService) {}

    public async getCurrencies(ranges: RangeType) {
        return this.exchangeService.find(ranges);
    }

    public async getCurrency(currency: string, ranges: RangeType) {
        return this.exchangeService.findOneByCode(currency, ranges);
    }
}
