import { HttpModule } from '@nestjs/axios';
import {
    BeforeApplicationShutdown,
    Module,
    OnApplicationBootstrap,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ScheduleModule } from '@nestjs/schedule';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AppConfigModule } from './config/config.module';
import { CronService } from './cron/cron.service';
import { CurrencyService } from './currency/currency.service';
import { PostgresModule } from './database/postgres.module';
import { DomainModule } from './domain/domain.module';

@Module({
    imports: [
        AppConfigModule,
        PostgresModule,
        DomainModule,
        ScheduleModule.forRoot(),
        HttpModule.registerAsync({
            inject: [ConfigService],
            useFactory: (config: ConfigService) => {
                return {
                    baseURL: 'https://api.currencyapi.com/v3',
                    timeout: config.get<number>('TIME_OUT') * 1000,
                    headers: {
                        apikey: config.get<string>('API_KEY'),
                    },
                };
            },
        }),
    ],
    controllers: [AppController],
    providers: [AppService, CronService, CurrencyService],
})
export class AppModule
    implements OnApplicationBootstrap, BeforeApplicationShutdown
{
    constructor(private readonly cronService: CronService) {}

    onApplicationBootstrap() {
        this.cronService.setTimeOut();
        this.cronService.setInterval();
    }

    beforeApplicationShutdown() {
        this.cronService.clearJobs();
    }
}
