import { SchedulerRegistry } from '@nestjs/schedule';
import { Test, TestingModule } from '@nestjs/testing';
import { AppConfigModule } from '../config/config.module';
import { CurrencyService } from '../currency/currency.service';
import { CronService } from './cron.service';

describe('Cron Service', () => {
    let app: TestingModule;
    let service: CronService;
    let schedulerRegistry: SchedulerRegistry;

    beforeAll(async () => {
        app = await Test.createTestingModule({
            imports: [AppConfigModule],
            providers: [
                CronService,
                {
                    provide: SchedulerRegistry,
                    useValue: {
                        addTimeout: jest.fn(),
                        addInterval: jest.fn(),
                        deleteTimeout: jest.fn(),
                        deleteInterval: jest.fn(),
                    },
                },
                {
                    provide: CurrencyService,
                    useValue: { latest: jest.fn() },
                },
            ],
        }).compile();

        service = app.get<CronService>(CronService);
        schedulerRegistry = app.get<SchedulerRegistry>(SchedulerRegistry);
    });

    afterAll((done) => {
        app.close().then(() => {
            done();
        });
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });

    it('should call addTimeout from schedule registry', () => {
        service.setTimeOut();
        expect(schedulerRegistry.addTimeout).toHaveBeenCalled();
    });

    it('should call addInterval from schedule registry', () => {
        service.setInterval();
        expect(schedulerRegistry.addInterval).toHaveBeenCalled();
    });

    it('should call deleteTimeout and deleteInterval from schedule registry', () => {
        service.clearJobs();
        expect(schedulerRegistry.deleteTimeout).toHaveBeenCalled();
        expect(schedulerRegistry.deleteInterval).toHaveBeenCalled();
    });
});
