export interface CronApi {
    setTimeOut(): void;
    setInterval(): void;
    clearJobs(): void;
}
