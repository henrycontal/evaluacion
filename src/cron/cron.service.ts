import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { SchedulerRegistry } from '@nestjs/schedule';
import { CurrencyService } from '../currency/currency.service';
import { CronApi } from './interfaces/cron.api';

export enum CRON_JOBS_NAMESPACES {
    CURRENCY_FIRST_EXECUTION,
    CURRENCY_INTERVAL_EXECUTION,
}

@Injectable()
export class CronService implements CronApi {
    private readonly logger = new Logger(CronService.name);

    constructor(
        private readonly config: ConfigService,
        private readonly schedulerRegistry: SchedulerRegistry,
        private readonly currency: CurrencyService,
    ) {}

    setTimeOut() {
        const callback = () => {
            this.logger.debug(`Executing first job once`);
            this.currency.latest();
        };

        const timeOut = setTimeout(callback, 0);
        this.schedulerRegistry.addTimeout(
            CRON_JOBS_NAMESPACES.CURRENCY_FIRST_EXECUTION.toString(),
            timeOut,
        );
    }

    setInterval() {
        const TIME_INTERVAL_MIN = this.config.get<number>('TIME_INTERVAL');

        const callback = () => {
            this.logger.debug(`Executing every ${TIME_INTERVAL_MIN} minutes`);
            this.currency.latest();
        };

        const interval = setInterval(callback, TIME_INTERVAL_MIN * 60 * 1000);

        this.schedulerRegistry.addInterval(
            CRON_JOBS_NAMESPACES.CURRENCY_INTERVAL_EXECUTION.toString(),
            interval,
        );
    }

    clearJobs() {
        this.schedulerRegistry.deleteTimeout(
            CRON_JOBS_NAMESPACES.CURRENCY_FIRST_EXECUTION.toString(),
        );

        this.schedulerRegistry.deleteInterval(
            CRON_JOBS_NAMESPACES.CURRENCY_INTERVAL_EXECUTION.toString(),
        );
    }
}
