--
-- PostgreSQL database dump
--

-- Dumped from database version 15.4 (Homebrew)
-- Dumped by pg_dump version 15.4 (Homebrew)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: currency_calls; Type: TABLE; Schema: public; Owner: econtreras
--

CREATE TABLE public.currency_calls (
    id integer NOT NULL,
    call_date timestamp without time zone,
    response_time double precision,
    status character varying(15)
);


ALTER TABLE public.currency_calls OWNER TO econtreras;

--
-- Name: currency_calls_id_seq; Type: SEQUENCE; Schema: public; Owner: econtreras
--

CREATE SEQUENCE public.currency_calls_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.currency_calls_id_seq OWNER TO econtreras;

--
-- Name: currency_calls_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: econtreras
--

ALTER SEQUENCE public.currency_calls_id_seq OWNED BY public.currency_calls.id;


--
-- Name: exchanges; Type: TABLE; Schema: public; Owner: econtreras
--

CREATE TABLE public.exchanges (
    id integer NOT NULL,
    code character varying(5),
    value double precision,
    last_updated_at timestamp without time zone
);


ALTER TABLE public.exchanges OWNER TO econtreras;

--
-- Name: exchange_id_seq; Type: SEQUENCE; Schema: public; Owner: econtreras
--

CREATE SEQUENCE public.exchange_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.exchange_id_seq OWNER TO econtreras;

--
-- Name: exchange_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: econtreras
--

ALTER SEQUENCE public.exchange_id_seq OWNED BY public.exchanges.id;


--
-- Name: currency_calls id; Type: DEFAULT; Schema: public; Owner: econtreras
--

ALTER TABLE ONLY public.currency_calls ALTER COLUMN id SET DEFAULT nextval('public.currency_calls_id_seq'::regclass);


--
-- Name: exchanges id; Type: DEFAULT; Schema: public; Owner: econtreras
--

ALTER TABLE ONLY public.exchanges ALTER COLUMN id SET DEFAULT nextval('public.exchange_id_seq'::regclass);


--
-- Name: currency_calls currency_calls_pkey; Type: CONSTRAINT; Schema: public; Owner: econtreras
--

ALTER TABLE ONLY public.currency_calls
    ADD CONSTRAINT currency_calls_pkey PRIMARY KEY (id);


--
-- Name: exchanges exchange_pkey; Type: CONSTRAINT; Schema: public; Owner: econtreras
--

ALTER TABLE ONLY public.exchanges
    ADD CONSTRAINT exchange_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

