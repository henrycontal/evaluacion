# Evaluación

![Coppel Logo](https://image4.owler.com/logo/coppel_owler_20210603_142646_original.png)

Aquí se encuentra el código fuente de la evaluación así como el getting started del proyecto. 

## Requerimientos

 1. Es necesario tener instalado una versión de nodejs >= 18.16. Es posible instalarlo desde su página oficial en [Node.js](https://nodejs.org/es).
 2. Posteriormente es necesario descargar el paquete de NestJs con el comando: `npm install -g @nestjs/cli`
 3. Descargar PostgreSQL desde su página oficial en [PostgreSQL](https://www.postgresql.org/download/). 

## Getting Started

Se necesita de agregar el archivo `.env` con las configuraciones iniciales del proyecto. A continuación se muestra un ejemplo:

```Properties
# CURRENCY API
CURRENCY_API_KEY=cur_live_iZqdAKteyVKCKw21uuRDDrHgli7G6CStjx6lPYMR

# CALL CONFIGURATION
TIME_INTERVAL_MIN=30
TIME_OUT_SEG=5

# DATABASE CREDENTIALS
DB_HOST=localhost
DB_PORT=5432
DB_USER=econtreras
DB_PASS=coppel-pg
DB_NAME=currencies
```

Posteriormente se necesita de instalar las dependencias del proyecto, esto lo podemos realizar con el siguiente comando:
```bash
$ npm i
```

Para ejecutar el servicio web es posible con el siguiente comando: 
```bash
$ npm run start
```

Para ejecutar el servicio web en modo desarrollo es posible con el siguiente comando:
```bash
$ npm run start:dev
```

Importa la base de datos desde el archivo `db.sql` en la raíz del directorio.

## Testing
Para ejecutar las pruebas unitarias es posible con el siguiente comando:
```bash
$ npm run test
```

Para obtener el coverage de las pruebas unitarias es posible con el siguiente comando:
```bash
$ npm run test:cov
```

